```mermaid
graph LR
    A[Square Rect] -- Link text --> B((Circle))
    A --> C(Round Rect)
    B --> D{Rhombus}
    C --> D
```

## Project Time

```plantuml
@startgantt

<style>
ganttDiagram {
  task {
    BackGroundColor GreenYellow
    LineColor Green 
    unstarted {
      BackGroundColor Fuchsia 
      LineColor FireBrick
    }
  }
}
</style>
[Prepare] as [P] lasts 15 days
note bottom
  ready ...
end note

[Sw deploy] as  [S1] on {Dim 80%} lasts 15 days
[Sindic] as  [S2] on {Yur 30%} lasts 25 days
[P] -> [S1]
[P] -> [S2]
 [S2] is colored in Fuchsia/FireBrick 
 [S1]  is 30% complete
 [Prototype completed] happens at [S1]'s end

@endgantt
```
## diagram


```plantuml 	
@startmindmap
* bin
** 1
*** 11
*** 10
** 0
*** 01
*** 00

left side

** A
*** AA
*** AB
** B
@endmindmap
```

## Component 

```plantuml
@startuml

skinparam interface {
  backgroundColor RosyBrown
  borderColor orange
}

skinparam component {
  FontSize 13
  BackgroundColor<<Apache>> Pink
  BorderColor<<Apache>> #FF6655
  FontName Courier
  BorderColor black
  BackgroundColor gold
  ArrowFontName Impact
  ArrowColor #FF6655
  ArrowFontColor #777777
}

() "Data Access" as DA
Component "Web Server" as WS << Apache >>

DA - [First Component]
[First Component] ..> () HTTP : use
HTTP - WS

@enduml

```

## Activity
```plantuml
@startuml
start
:starting progress;
#HotPink:reading yml + tplt files
pandoc automatically generates at this point!;
#AAAAAA:creating the doc;
@enduml
```

## EA Archimate

```plantuml
@startuml
skinparam rectangle<<behavior>> {
	roundCorner 25
}
sprite $bProcess jar:archimate/business-process
sprite $aService jar:archimate/application-service
sprite $aComponent jar:archimate/application-component

rectangle "Handle claim"  as HC <<$bProcess>><<behavior>> #Business
rectangle "Capture Information"  as CI <<$bProcess>><<behavior>> #Business
rectangle "Notify\nAdditional Stakeholders" as NAS <<$bProcess>><<behavior>> #Business
rectangle "Validate" as V <<$bProcess>><<behavior>> #Business
rectangle "Investigate" as I <<$bProcess>><<behavior>> #Business
rectangle "Pay" as P <<$bProcess>><<behavior>> #Business

HC *-down- CI
HC *-down- NAS
HC *-down- V
HC *-down- I
HC *-down- P

CI -right->> NAS
NAS -right->> V
V -right->> I
I -right->> P

rectangle "Scanning" as scanning <<$aService>><<behavior>> #Application
rectangle "Customer admnistration" as customerAdministration <<$aService>><<behavior>> #Application
rectangle "Claims admnistration" as claimsAdministration <<$aService>><<behavior>> #Application
rectangle Printing <<$aService>><<behavior>> #Application
rectangle Payment <<$aService>><<behavior>> #Application

scanning -up-> CI
customerAdministration  -up-> CI
claimsAdministration -up-> NAS
claimsAdministration -up-> V
claimsAdministration -up-> I
Payment -up-> P

Printing -up-> V
Printing -up-> P

rectangle "Document\nManagement\nSystem" as DMS <<$aComponent>> #Application
rectangle "General\nCRM\nSystem" as CRM <<$aComponent>>  #Application
rectangle "Home & Away\nPolicy\nAdministration" as HAPA <<$aComponent>> #Application
rectangle "Home & Away\nFinancial\nAdministration" as HFPA <<$aComponent>>  #Application

DMS .up.|> scanning
DMS .up.|> Printing
CRM .up.|> customerAdministration
HAPA .up.|> claimsAdministration
HFPA .up.|> Payment

legend left
Example from the "Archisurance case study" (OpenGroup).
See
====
<$bProcess> :business process
====
<$aService> : application service
====
<$aComponent> : application component
endlegend
@enduml

```

## Data Flow

```plantuml
  TPYaml -> PandHTM -> TPHTM
 TPMd -> PandHTM
 TPHTM  -> Pandoc -> Dochtm
Datyaml -> Pandoc 
 
```
or 
```mermaid
graph LR
   setup -->|TPYaml| PandHTM 
   PandHTM -->|TPHTM| Pandoc
   user -->|TPMd| PandHTM
   Pandoc -->|Dochtm| review
   user2 -->|Datyaml| Pandoc  
```

or 

```mermaid
graph TD
    A[setup] -- TPMD --> B((pandoc HT))
    u[user] -- TPYAML --> B 
    B -- HTM --> C(pand 2)
    C --> D{result}
```

## EA

 seq diagram 

```plantuml
@startuml
actor Alice #green
actor Eve #red
actor Bob #blue
' The only difference between actor 'and participant is the drawing
 participant "another\nlong name" as L #9F9

Alice->Bob: Authentication Request
Bob->Alice: Authentication Response
Bob->L: Log transaction

Alice->Eve: original msg
Eve->Bob: tampered msg
@enduml
```



and

```plantuml
@startuml
:Main Admin: as Admin
(Use the application) as (Use)
User -> (Start)
User --> (Use)
Admin ---> (Use)
note right of Admin : with root pass.
note right of (Use)
 recognition sw
 by tesseract
end note
note "many can \nuse the scan" as N2
(Start) .. N2
N2 .. (Use)
@enduml
```

FSM:

```graphviz
digraph finite_state_machine {
  rankdir=LR;
  node [shape = doublecircle]; LR_0 LR_3 LR_4 LR_8;
  node [shape = circle];
  LR_0 -> LR_2 [ label = "SS(B)" ];
  LR_0 -> LR_1 [ label = "SS(S)" ];
  LR_1 -> LR_3 [ label = "S($end)" ];
  LR_2 -> LR_6 [ label = "SS(b)" ];
  LR_2 -> LR_5 [ label = "SS(a)" ];
  LR_2 -> LR_4 [ label = "S(A)" ];
  LR_5 -> LR_7 [ label = "S(b)" ];
  LR_5 -> LR_5 [ label = "S(a)" ];
  LR_6 -> LR_6 [ label = "S(b)" ];
  LR_6 -> LR_5 [ label = "S(a)" ];
  LR_7 -> LR_8 [ label = "S(b)" ];
  LR_7 -> LR_5 [ label = "S(a)" ];
  LR_8 -> LR_6 [ label = "S(b)" ];
  LR_8 -> LR_5 [ label = "S(a)" ];
}
```

and C4 architectures:

```c4plantuml
@startuml
!include C4_Context.puml

title System Context diagram for Internet Banking System

Person(customer, "Banking Customer", "A customer of the bank, with personal bank accounts.")
System(banking_system, "Internet Banking System", "Allows customers to check their accounts.")

System_Ext(mail_system, "E-mail system", "The internal Microsoft Exchange e-mail system.")
System_Ext(mainframe, "Mainframe Banking System", "Stores all of the core banking information.")

Rel(customer, banking_system, "Uses")
Rel_Back(customer, mail_system, "Sends e-mails to")
Rel_Neighbor(banking_system, mail_system, "Sends e-mails", "SMTP")
Rel(banking_system, mainframe, "Uses")
@enduml
```


## OLD NOT Valid  DF

```mermaid
digraph dfd{	
	node[shape=record]
	store1 [label="<f0> left|<f1> Some data store"];
	proc1 [label="{<f0> 1.0|<f1> Some process here\n\n\n}" shape=Mrecord];
	enti1 [label="Customer" shape=box];
	store1:f1 -> proc1:f0;
	enti1-> proc1:f0;
}
```
