## PACS

The Linux Image processing tools enhanced in this repo https://gitlab.com/crlhyn/scan-digitize/PACS.md 
apply also to picture archiving and communication system (PACS) for processing medical images.


![pic 1](https://upload.wikimedia.org/wikipedia/en/c/ce/Pacs1.jpg)

and


![pic 2](https://upload.wikimedia.org/wikipedia/en/d/d8/Pacs2.jpg)


### Refererences

In this area few open sources we can use also: (since 2015) from the rapid creation of data collection instruments and data analysis, visualization, and reporting using epidemiologic methods. Epi Info™,


- [] Epi Info is public domain statistical software for epidemiology developed by Centers for Disease Control and Prevention (CDC) in Atlanta, Georgia (USA). win. http://wwwn.cdc.gov/epiinfo/	electronic survey creation, data entry, and analysis. 

- [] Open Dental, previously known as Free Dental, is a Practice Management Software licensed under the GNU General Public License.[10] It is written in the C# programming language / complete patient records (HIPAA compliant)

- Cottage Med is electronic medical record (EMR) software based on FileMaker, created by Caring in Community 501(c)3,[1] a Massachusetts hill town nonprofit organization. http://www.cottagemed.org



- FreeMED is an opensource electronic medical record system based on Linux, Apache, MySQL and PHP (commonly referred to as LAMP).
in PHP, and makes heavy use of SQL, favoring the MySQL database engine. 
Electronic Medical Record Information Translation and Transmission on XML.
http://github.com/freemed/remitt   git clone git://github.com/freemed/remitt.git


- GNU Health is a Free Health and Hospital Information System that provides the following functionality:
        -     Electronic Medical Record (EMR)
        -     Hospital Information System (HIS)
        -     Health Information System
multi-platform  (Linux, FreeBSD, MS Windows) and different database management systems (PostgreSQL). It's written in Python 

- GaiaEHR[2] is a Medical practice management software which also supports Electronic Medical Records. GaiaEHR is one of the various open source technologies.[3][4]  LAMP  http://gaiaehr.org/


- Mirth Connect is a cross-platform HL7 interface engine that enables bi-directional sending of HL7 messages between systems and applications over multiple transports available under the Mozilla Public License (MPL) 1.1 license.

- ClearHealth is an Open Source practice management (PM) and electronic medical records (EMR/EHR/PHR) system available under the GNU General Public License.



- Laika ORU: was released in September 2008 to test the interoperability of HL7 2.5.1 lab messages. Laika ORU can be used with Mirth, an open source health informatics messaging package, to manage the routing of HL7 2.5.1 lab messages with Laika.


### Formats

 Health Level-7 or HL7 refers to a set of international standards for transfer of clinical and administrative data between software applications used by various healthcare providers.
 
 
 The following is an example of an admission message. MSH is the header segment, PID the Patient Identity, PV1 is the Patient Visit information, etc. The 5th field in the PID segment is the patient's name, in the order, family name, given name, second names (or their initials), suffix, etc. Depending on the HL7 V2.x standard version, more fields are available in the segment for additional patient information.

    MSH|^~\&|MegaReg|XYZHospC|SuperOE|XYZImgCtr|20060529090131-0500||ADT^A01^ADT_A01|01052901|P|2.5
    EVN||200605290901||||200605290900
    PID|||56782445^^^UAReg^PI||KLEINSAMPLE^BARRY^Q^JR||19620910|M||2028-9^^HL70005^RA99113^^XYZ|260 GOODWIN CREST DRIVE^^BIRMINGHAM^AL^35209^^M~NICKELL’S PICKLES^10000 W 100TH AVE^BIRMINGHAM^AL^35200^^O|||||||0105I30001^^^99DEF^AN
    PV1||I|W^389^1^UABH^^^^3||||12345^MORGAN^REX^J^^^MD^0010^UAMC^L||67890^GRAINGER^LUCY^X^^^MD^0010^UAMC^L|MED|||||A0||13579^POTTER^SHERMAN^T^^^MD^0010^UAMC^L|||||||||||||||||||||||||||200605290900
    OBX|1|NM|^Body Height||1.80|m^Meter^ISO+|||||F
    OBX|2|NM|^Body Weight||79|kg^Kilogram^ISO+|||||F
    AL1|1||^ASPIRIN
    DG1|1||786.50^CHEST PAIN, UNSPECIFIED^I9|||A

HL7 v2.x has allowed for the interoperability between electronic Patient Administration Systems (PAS), Electronic Practice Management (EPM) systems, Laboratory Information Systems (LIS), Dietary, Pharmacy and Billing systems as well as Electronic Medical Record (EMR) or Electronic Health Record (EHR) systems. Currently, the HL7 v2.x messaging standard is supported by every major medical information systems vendor in the United States.[7]
 
 Fast Healthcare Interoperability Resources (FHIR)
Main article: Fast Healthcare Interoperability Resources

Fast Healthcare Interoperability Resources is a draft standard from HL7 International designed to be easier to implement, more open and more extensible than version 2.x or version 3. It leverages a modern web-based suite of API technology, including a HTTP-based RESTful protocol, HTML and Cascading Style Sheets for user interface integration, a choice of JSON or XML for data representation, OAuth for authorization and ATOM for query results.[12]



  Electronic Health Records (EHR) System.  

The District Health Information Software (DHIS) is used in more than 40 countries around the world. DHIS is an open source software platform for reporting, analysis and dissemination of data for all health programs, developed by the Health Information Systems Programme (HISP). The core development activities of the DHIS 2 platform (see note on releases and versions further down) are coordinated by the Department of Informatics at the University of Oslo, and supported by NORAD, PEPFAR, The Global Fund to Fight AIDS, Tuberculosis and Malaria, UNICEF and the University of Oslo.
https://www.dhis2.org/
