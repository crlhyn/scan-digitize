<meta charset="UTF-8">

# Analyzed Doc 

###  PUBLIC  - Digitally-Created - 31stReportUkraine-ru.pdf  
	
This public example shows text recognized from embedded-images, you can see if differs or matches your goals.
	
  - Tested with embedded images in a public UN document  [https://www.ohchr.org/_layouts/15/WopiFrame.aspx?sourcedoc=/Documents/Countries/UA/31stReportUkraine-ru.pdf](https://www.ohchr.org/_layouts/15/WopiFrame.aspx?sourcedoc=/Documents/Countries/UA/31stReportUkraine-ru.pdf&action=default&DefaultItemOpen=1) 
  - To simplify usage, the tools expects 1 input (a PDF you receive or copy from usb)  and generates 3 outputs.
  - The tool produced a tuned-PDF, a text, and this HTM with russian words recognized (before & after color-light tuning). 

Consider this just a  best-effort tool for help rather than a "solution" . Because :
	
 - these tools runs on linux, Those without linux production support can only rely on a best-effort help from linux-experienced colleagues.
 - text-recognition can be stochastic unpredictable on image quality, light, colors,… even for humans, seen in enclosed [Page-33(10) example]*
 
Now, you can test with sensible documents, by simply copying from usb to standalone ( simpler than pluging scanners ).  

  - For priorities, consider how many troubled-docs you got in digital-PDF (those where you can highlight text ) and
 - how many trouble-docs you received in paper (there we apply the Scanned-2recognizeImages in  [other-tools]) 
 - please remember to keep security involved if moving the standalone around (and step 4 to delete files after usage) 
      
 
## Usage steps


This example copied the 1 input, then 2) processed it, copied 3 ouputs, and 4 deleted files. 

1. In Linux File-Explore, copy the input from USB into /opt/ocr/work

2. In the Linux Desktop, click on the tool needed per each case.
    - we clicked:     Digital-2Images-2colorLight-2ocr 

3. In the Linux file-Explorer,  copy the 3 outputs  from /opt/ocr/work into a safer USB  
    - a TXT file with all rusian text found by free-OCR in the images  (for any text editor )
    - a HTM file (same as above for any local WEB browser )
    - a PDF with the images zoomed / rotated / tuned in bright/color for any-OCR

4. Delete files in /opt/ocr/work/  ,   for security 

  

```mermaid
graph LR; 

 Standalone: --> 1CopyFromUsb --> 2ClickTool --> 3copyResults --> 4delete

  PDF --> usbCopy --> Tool1 --> HTM-report;
   Tool1--> TXT-report;
  Tool1   --> PDF-tuned;
    PDF --> upload --> Tool1;

     Scan --> image --> Tool2-->Zoom;

```

## Page-33(10) example
- In the map in page 33, The tool attempted 2 combinations of colors & contrast,
- One option found Закарпатська Carpathia (left), others missed it but found other regions.

  Comparing Carpathia in pages 5 & 33 we can see in each method can do it better or worst depending on the map color environments

 ![ Brightleft-vs-Hue-Right  ](Brightleft-vs-Hue-Right.png "comparing ")
 


 
# Automatically-Generated Results:

The tool automatically finds any digitally-attached  images , considering only those over 9K size:

	page   num  type   width height color comp bpc  enc interp  object ID x-ppi y-ppi size ratio
	--------------------------------------------------------------------------------------------
	   1     0 image    1189  1681  rgb     3   8  jpeg   no      2613  0   145   145  267K 4.6%
	   5    32 image    3509  2481  cmyk    4   8  jpeg   no        20  0   481   241  503K 1.5%
	  11    33 image    1332  1303  rgb     3   8  jpeg   no        37  0   193   193  187K 3.7%
	  12    34 image    2004  2911  rgb     3   8  jpeg   no        40  0   276   276  544K 3.2%
	  13    37 image    1654   889  rgb     3   8  jpeg   no        43  0   317   317  170K 3.9%
	  14    44 image     828   964  rgb     3   8  jpeg   no        47  0   320   320  147K 6.3%
	  14    45 image     828   738  rgb     3   8  jpeg   no        48  0   286   286  218K  12%
	  21    74 image    1654  1099  rgb     3   8  jpeg   no        73  0   271   271  469K 8.8%
	  24    79 image    1655  1243  icc     4   8  jpeg   no        84  0   235   133  280K 3.5%
	  33   120 image    3508  2481  cmyk    4   8  jpeg   no       115  0   516   258  764K 2.2%


		 
### PAGE-IMG x-001-000.jpg 
            (ФФУ у ое ЧИТ						      |	(ФФУ ое ИМИ
            С ПРАВАМИ ЧЕЛОВЕКА						С ПРАВАМИ ЧЕЛОВЕКА
            В УКРАИНЕ							В УКРАИНЕ
            
            1 АВГУСТА 2020 - 31 ЯНВАРЯ 2021					1 АВГУСТА 2020 - 31 ЯНВАРЯ 2021
            
             								 
            
            Ге м ть АИ.						      |	и ) Се и та.
            К ПРАВА ЧЕЛОВЕКА					      |	К АННЫ
            
            ОДО							      |	Л ОИ «Л
            
            								
                                                                        
### PAGE-IMG x-005-032.jpg 
            Мониторинговая миссия ООН по правам человека в Украине	      |	НОМАМ ВИСНТ$
            							      >	ОЕНСЕ ОЕТНЕ НИЕН СОМНИЗЗИЮНЕЕ по состоянию на 15 февраля 2021
            
            УКРАИНА							      |	ОМТЕР МАПОМ$ —
            							      >	\ Мониторинговая миссия ООН по правам человека в Украине
            
            Столица							      |	 
            Государственная граница					      <
            Границы областей					      <
            ет:							      <
            
            Я-то ее) ВИ ВА						      |	БЕЛАРУСЬ
            							      >
            							      >	       
            							      >	  
            							      >
            							      >	    
            							      >
            							      >	   
            							      >	  
            							      >
            							      >	 
            							      >
            							      >	 
            							      >
            							      >	 
            							      >
            							      >	ВОЛЫНСКАЯ *. г . 7
            							      >	ПОЛЬША 5 и г ЧЕРНИГОВСКАЯ }
            							      >	‚ К УРОВЕНСКАЯ) ь АИ \ АО
            							      >	к „=“, ЖИТОМИРСКАЯ © и ` к Я
            							      >	ЛЬВОВСКАЯ Е. : А ты
            							      >	Я \ КИЕВСКАЯ №“ Г
            							      >	ТЕРНОПОЛЬСКАЯ и о ПОЛТАВСКАЯ “©.
            							      >	е ? \ . — у КРАИНА \ › ХАРЬКОВСКАЯ
            							      >	3 ИВАНО- 2. : г | ЧЕРКАССКАЯ Сл ТВ,
            							      >	``ФРАНКОВСКАЯ * } ВИННИЦКАЯ ^ рой ЗЫ бы У мы
            							      >	ЗАКАРПАТСКАЯ _. А ал К ИР ся №4 ма
            							      >	ЯЕРНОВИЦКАЯ КИРОВОГРАДСКАЯ ``  ДНЕПРОПЕТРОВСКАЯ
            							      >	} <, ДОНЕЦКАЯ
            							      >	РУМЫНИЯ ` НИКОЛАЕВСКАЯ © ^^ ^^ 7 щ ях
            							      >	= и >> ЗАПОРОЖСКАЯ 4 п. и
            							      >	ОДЕССКАЯ < \ АЙ О
            							      >	со = ХЕРСОНСКАЯ 3. у * ГОВОниОКАя
            							      >	Ч > ци’ -—_ ФЕДЕРАЦИЯ
            							      >	и `` ” „—%
            							      >	и ГУ
            							      >	Столица 7
            							      >	Государственная граница = Рой <. \
            							      >	^ РР \
            							      >	еееыы Границы областей | Зернаеморе Я 2 И к 5
            							      >	у ” “ >
            							      >	Г\\_>-> Линия соприкосновения . | ^^ КРЫМ а
            							      >	7 / \ в <
            							      >	Местонахождение офисов ММПЧУ и—^ и. м " д
            							      >	р „СЕВАСТОПОЛЬ х
            							      >	2 с 2 ви
            							      >	/ ^^. ``
            							      >
            							      >	 
            							      >
            							      >	 
            							      >
            							      >	 
            							      >
            							      >	Границы и названия, приведенные на данной карте, а также испо
            							      >
            							      >	 
            							      >
            							      >	Создано: 15 февраля 2021 года Источник: ММПЧУ, УВКПЧ Контакты
            
            								
                                                                        
### PAGE-IMG x-011-033.jpg 
               							      |	  
            							      >	 
            							      >	 
            							      >	 
            							      >
            							      >	    
            							      >
            							      >	| Пани
            							      >	ИНТЕРЕСАХ.
            							      >	УСТОЙЧИВОГО РАЗВИТИЯ
            							      >
            							      >	      
            							      >	 
            							      >	 
            
            ОЦЕЛИ							      |	и ОН
            							      >	И ЭФФЕКТИВНЫЕ
            							      >	а
            							      >
            							      >	     
            							      >	  
            							      >	      
            							      >
            							      >	| СОХРАНЕНИЕ
            							      >	НИНА
            							      >
            							      >	    
            							      >	 
            							      >
            							      >	ХОРОШЕЕ ЗДОРОВЬЕ
            							      >	ДА
            							      >
            							      >	       
            							      >
            							      >	 
            							      >	 
            
            В ОБЛАСТИ						      |	| ИЗ
            							      >	нА
            							      >	НН
            							      >
            							      >	 
            							      >	 
            							      >
            							      >	    
            							      >
            							      >	ук и
            							      >
            							      >	ТА
            							      >
            							      >	&
            							      >
            							      >	| ОТВЕТСТВЕННОЕ
            							      >	НИЗ
            							      >	ЕН
            							      >
            							      >	в  ВОБЛАСТИ
            УСТОИЧИВОГО							УСТОИЧИВОГО
            
            РАЗВИТИЯ							РАЗВИТИЯ
            
             								 
            							      >	   
            							      >	   
            							      >	    
            
             							      |	нЕ
            							      >	МДА
            
            							      >	р
            							      >
            							      >	     
             								 
            							      >
            							      >	  
            							      >
            							      >	ДОСТОЙНАЯ РАБОТА
            							      >	НЫ
            							      >	РОСТ
            							      >
            							      >	  
            
            								
                                                                        
### PAGE-IMG x-012-034.jpg 
             								 
            
            & (№ УКРАИНА: ЖЕРТВЫ СРЕДИ ГРАЖДАНСКИХ ЛИЦ В РЕЗУЛЬТАТЕ АКТИВ |	и \ УКРАИНА: ЖЕРТВЫ СРЕДИ ГРАЖДАНСКИХ ЛИЦ В РЕЗУЛЬТАТЕ АКТИВН
            © В2020 ГОДУ						      |	У ХУ В2020 ГОДУ
            
             								 
            
                							      |	 
              								  
            							      |	 
            							      >	 
             								 
            
            Станица Луганская					      |	     
            							      |	     
               							      |	  
            							      <
            РОССИЙСКАЯ						      <
            ФЕДЕРАЦИЯ						      <
            							      <
            Условные обозначения					      <
            							      <
            Погибшие гражданские лица				      <
            							      <
            ©!							      <
            							      <
            Раненые гражданские лица				      <
            							      <
            \ Азовское море : © ›1					      <
            
            							      >	$ \
            							      >	т №
            							      >	2 им
            							      >	ди з ‘учу —
            							      >	(
            							      >	гы | 1
            							      >	—и ‘4 —
            							      >	и” З < / 7 <. с
            							      >	\ “Я Г Счастье
            							      >	\ ‹
            							      >	о 7
            							      >	5 —
            							      >	== УКРА и НА Ра @ нь
            							      >	—— т олубовка
            							      >	ь. ^
            							      >	2
            							      >	и
            							      >	| зи
            							      >	А
            							      >	р \
            							      >	я 'Майорское
            							      >	| р уе
            							      >	=
            							      >	\ (© Широкая Балка | И
            							      >	\ 77 - й
            							      >	- Пантелеймоновка | > ЛУГАНСКАЯ ОБЛАСТЬ
            							      >	г Е : ‹
            \								\
            5\ Открытые КПВВ					      |	\
            й							      |	<) |
            							      |	а
            							      >	Красногоровк.
            							      >	Марьинка
            							      >	Александровка | у. рьинка
            							      >	} ©.
            							      >	у
            							      >	\
            							      >	Я
            							      >	д
            							      >	\
            							      >	Ал
            							      >	`
            							      >	—=3 |
            							      >	га Условные обозначения
            							      >	“<
            							      >	м , с РА , Погибшие гражданские лица
            							      >	и к о!
            							      >	<
            							      >	У Раненые гражданские лица
            							      >	2
            							      >	г
            							      >	`
            							      >	\ Азовское море 8 е 1
            							      >	Открытые КПВВ
            Закрытые КПВВ							Закрытые КПВВ
            							      |	# 8 > шшшыи Линия соприкосновения
            = Линия соприкосновения					      |	/ \
            —-—- Области						      |	/ \ —-- Области
            - Районы						      |	|
            Населенные пункты					      |	\ ] Районы
            <—— Главная дорога					      |	№ ” Населенные пункты
            							      |	—. Главная дорога
            0 25 50 км —  Второстепенная дорога				0 25 50 км —  Второстепенная дорога
            
            нии							      <
            							      <
            Границы и названия, приведенные на данной карте, и использова	Границы и названия, приведенные на данной карте, и использова
            или согласия ООН.						или согласия ООН.
            
            Дата создания: 25 февраля 2021 г. Автор: ОНСНК ЕК$ Источник:  |	Дата создания: 25 февраля 2021 г. Автор: ОНСНК ЕК$ Источник: 
            
            								
                                                                        
### PAGE-IMG x-013-037.jpg 
             								 
            
            ХРОНОЛОГИЯ ЖЕРТВ СРЕДИ ГРАЖДАНСКОГО НАСЕЛЕНИЯ ст января 2018  |	ХРОНОЛОГИЯ ЖЕРТВ СРЕДИ ГРАЖДАНСКОГО НАСЕЛЕНИЯ с 1 января 2018
            
             								 
            
            60 № погибшие							60 № погибшие
            
            							      >	        
            							      >
            Ш пивные, 1Тиюля 29 декабря 21 июля 27 июля			Ш пивные, 1Тиюля 29 декабря 21 июля 27 июля
            р о «хлебное» «новогоднее» «бессрочное» дополнительные меры п	р о «хлебное» «новогоднее» «бессрочное» дополнительные меры п
            9 перемирие перемирие перемирие соблюдению перемирия	      |	5 перемирие перемирие перемирие соблюдению перемирия
            40 1 сентября | 8 марта					      |	40 38 Т сентября 8 марта
            «ШКОЛЬНОЕ» «весеннее»					      |	й «Школьное» «весеннее»
            30 перемирие | перемирие 30				      |	30 перемирие ‘перемирие 30
            22							      |	20 1 я
            20 о 19							      |	14 5 | й в
            4 р 13 р						      |	ий | || оп т ий й й 10 12 п
            1 т п д							      |	| 2 Ща
            87 т 8							      |	Н] || | 19. И 222 й м7 |2 Ма ИИ 1 1 0 ия
            ] 3.05 5 Идид 6 и5Й 6					      |	мух РУ - мии ими мии ми им ее ууу гии иими м
            За НН:							      |
            ИВ, ИИ а						      |	У \
            0150 03 04 05 06 07 08 09 10 11 12,0102 03 04 05 06 07 08 09  |	01 02 03 04 05 06 о 08 09 10 11 12 01 02 03 04 05 06 07 08 09
            							      >	2018 2019 2020 2021
            
             								 
            
            Дата создания: 24 февраля 2021 г. Источник: ММПЧУ УВКПЧ		Дата создания: 24 февраля 2021 г. Источник: ММПЧУ УВКПЧ
            
            								
                                                                        
### PAGE-IMG x-014-044.jpg 
             								 
            
            КОЛИЧЕСТВО ГРАЖДАНСКИХ ЛИЦ,					КОЛИЧЕСТВО ГРАЖДАНСКИХ ЛИЦ,
            
            ПОГИБШИХ В СВЯЗИ С КОНФЛИКТОМ					ПОГИБШИХ В СВЯЗИ С КОНФЛИКТОМ
            за 2014-2020 годы						за 2014-2020 годы
            
             								 
            
            2500								2500
            2,084								2,084
            
            2000								2000
            1500								1500
            1000 954						      |	1000
            500								500
            , 112 ИР 55 27 26					      |	ны шш 55 27 26
            ими м м							      <
            
            Дата создания: 24 февраля 2021 г. Источник: ММПЧУ УВКПЧ	      |	У У
            							      >	214 2018 роте 2017 отв 2019 2020
            
             								 
            							      >
            							      >	Дата создания: 24 февраля 2021 г. Источник: ММПЧУ УВКПЧ
            
            								
                                                                        
### PAGE-IMG x-014-045.jpg 
             								 
            
            ПРИЧИНЫ ЖЕРТВ СРЕДИ ГРАЖДАНСКОГО				ПРИЧИНЫ ЖЕРТВ СРЕДИ ГРАЖДАНСКОГО
            НАСЕЛЕНИЯ ЗА ПЕРИОД СТ ЯНВАРЯ ПО			      |	НАСЕЛЕНИЯ ЗА ПЕРИОД С 1 ЯНВАРЯ ПО
            31 ДЕКАБРЯ2020 ГОДА					      <
            							      <
             							      <
            							      <
            Артиллерийские обстрелы и огонь РРР 61			      <
            из стрелкового оружия и легких 3%			      <
            
            вооружений						      |	31 ДЕКАБРЯ2020 ГОДА
            							      <
            Инциденты, связанные с минами /				      <
          
            обращением с взрывоопасными = И				      |	ОИ ИИ
            17							      |	иные ар ПИИИИИИИИИИИИИЙ 59
            
            пережитками войны						пережитками войны
            
            59							      |	 
            							      >
            							      >	‚
            							      >	6-7 |1
            
            И							      |	Удары с БПЛА +4 +
            							      >	э [|1
            
            Дорожные инциденты с  - 12				      |	Дорожные инциденты с 3 12
            
            участием военных						участием военных
            убийства „< Ш погибшие					      |	. Ш погибшие
            1 Ш раненые						      |	Убийства .
            							      >	1 раненые
            
            Дата создания: 24 февраля 2021 г. Источник: ММПЧУ УВКПЧ		Дата создания: 24 февраля 2021 г. Источник: ММПЧУ УВКПЧ
            
             								 
            
            								
                                                                        
### PAGE-IMG x-021-074.jpg 
             								 
            
            РОЗСЛИДУВАННЯ ТА СУДОВЕ ПЕРЕСЛ!ДУВАННЯ КАТУВАНЬ ТА ЖОРСТОКОГО	РОЗСЛИДУВАННЯ ТА СУДОВЕ ПЕРЕСЛ!ДУВАННЯ КАТУВАНЬ ТА ЖОРСТОКОГО
            ЗЕСТОРОНИ СШВРОЫТНИКВ ПРАВООХОРОННИХ ОРГАНПВ У 2018-2020 РОКА |	З! СТОРОНИ СПИВРОЫТНИК!В ПРАВООХОРОННИХ ОРГАНПВ У 2018-2020 Р
            							      >
            							      >	 
            
            110								110
            
            90								90
            
            70								70
            
            50								50
            
            30								30
            
            Ш 2020								Ш 2020
            Ш середньор/чна клькгсть в 2018-2019 рр.		      |	Ш середньор/чна кльксть в 2018-2019 рр.
            							      <
               							      <
            
            збльшення на							збльшення на
            
            							      >	  
            							      >
             								 
            
            140%								140%
            збльшення на							збльшення на
            з@льшення на 63% ,					      |	збльшення на 63% ,
            372% збльшення на						372% збльшення на
            							      >
            440%								440%
            И М”							      |
            Юльксть Юльюсть ствробтникв Юльюсть спвробтникв — Кльксть спв |	й ИИ
            спвробтникв правоохоронних органв, — правоохоронних органв,   |	ЮКльксть Юльюсть ствробтникв Кльксть ствробтникв — Кльюсть ст
            правоохоронних яким було повдомлено — яким було повдомлено —  |	спивробтникв правоохоронних орган, — правоохоронних органе,  
            органгв, яким було про подозру за частиной 2 про тдозру за ча |
            пов!домлено про стати! 365 КК 3 стати 365 КК 371 КК	      |	правоохоронних
            п!дозру за статтею (перевищення влади або  (перевищення влади |	органйв, яким було
            127 КК (катування) службових обов'язкав, яке службових повнов |	пов/домлено про
            супроводжувалося яке спричинило тяжк!			      |	пидозру за статтею
            насильством) наслдки)					      |	127 КК (катування)
            							      >
            							      >	яким було пов'домлено
            							      >
            							      >	про подозру за частиной 2
            							      >
            							      >	стат! 365 КК
            							      >	(перевищення влади або
            							      >
            							      >	службових обов'язкв, яке
            							      >
            							      >	супроводжувалося
            							      >	насильством)
            							      >
            							      >	яким було повдомлено
            							      >	про пдозру за частиной
            							      >	Зстатп 365 КК
            							      >	(перевищення влади або
            							      >	службових повноважень,
            							      >	яке спричинило тяжк!
            							      >	наслдки)
            							      >
            							      >	яким було повдомлено
            							      >	про пгдозру за статтею
            							      >	371 КК
            							      >
            							      >	(незаконне затримання
            							      >	або тримання п вартою)
            
             								 
            
            Дата створення: 24 лютого 2021 р. Джерело: Оф!с Генерального  |	Дата створення:
            							      >
            							      >	24 лютого 2021 р. Джерело: Оф/с Генерального прокурора
            
            								
                                                                        
### PAGE-IMG x-024-079.jpg 
             								 
            
            ПРИГОВОРЫ СУДОВ ПЕРВОЙ ИНСТАНЦИИ ПО ДЕЛАМ, СВЯЗАННЫМ С КОНФЛИ	ПРИГОВОРЫ СУДОВ ПЕРВОЙ ИНСТАНЦИИ ПО ДЕЛАМ, СВЯЗАННЫМ С КОНФЛИ
            							      >
            1 августа 2020 года - 31 января 2021 года			1 августа 2020 года - 31 января 2021 года
            
            те:							      |	всего
            							      >	приговоров*
            							      >	143
            							      >	производство производство
            							      >	1 арзепНа в общем
            							      >	порядке
            							      >	2 оправдательных
            							      >	я
            							      >	Ай
            							      >	Виды } в %,
            							      >	приговоров 2
            							      >
            							      >	т
            
            Виды							      |	Все обвинительные 141 обвинительный
            приговоров						      <
            
            Основания для						      |	08 ИИ от
            							      >	Основания для ВИНОВНОСТИ
            							      >	Признание ВИНЫ
            постановления							постановления
            приговоров: 4 Г						      |
            							      >	Вина доказана стороной
            							      >	приговоров: и
            							      >
            							      >	4 1 обвинения
            							      >
            							      >	100 75 50 25 0
            							      >
            							      >	Дата создания: 15 февраля 2021 года
            							      >	Источник: УВКПЧ ММПЧУ
            							      >
            							      >	 
            							      >
            							      >	Суть обвинения
            							      >
            							      >	Участие в организации голосования в
            							      >	самопровозглашенных ‘республиках” **
            							      >
            							      >	ПАНК |88
            							      >
            							      >	Публикация В социальных сетях призывов,
            							      >	посягающих на территориальную
            							      >	целостность Украины
            							      >
            							      >	2 20
            							      >
            							      >	Участие в вооруженных группах
            							      >	самопровозглашенных ‘республик”
            							      >
            							      >	Й 17
            							      >
            							      >	Охрана блокпостов
            							      >
            							      >	14
            							      >
            							      >	Финансирование самопровозглашенных
            							      >	‘республик
            							      >
            							      >	Й 6
            							      >	‚Другое ^""
            							      >
            							      >	3
            							      >
            							      >	>
            
            								
                                                                        
### PAGE-IMG x-033-120.jpg 
            Информация о приютах, доступных для бездомных людей в областя	Информация о приютах, доступных для бездомных людей в областя
            							      >	по состоянию на декабрь 2020 года
            
             								 
            
             							      |	   
            
             							      |	о
            
             							      |	8: р м
            							      >	; 1 [9/72
            
                							      |	Ровно
                							      <
             							      <
              							      <
             							      <
            
              								  
            							      <
              								  
            
            ео							      |	Ия
            Лук | р Сим						      <
            —_ ИТ							      <
            ЕТ) и							      <
            —_ Житомир						      <
            И КИЕВСКАЯ Иа						      <
            о [еле							      <
            вдо-Франковек 7 | а,					      <
            Оля ВИННИЦКАЯ |						      <
            А Славянск						      <
            ЗАКАРПАТСКАЯ | | | }					      <
            МА Дружковка Констант®					      <
            ТЕ ИО В _^\авеиное Плот |				      <
            							      <
            ОО							      <
            
            | ? |							      |	    
            И К № в ЗАПОРОЖСКАЯ Ве					      <
            
            ” Нееолеев ^						      |	©
            
             							      |	Львов
            
             							      |	ой. › и у Харьков
            							      >	© ” Я ® Полтава
            							      >	Хмельницький — ыы |
            							      >	Я и” Е. в
            							      >	а р
            							      >	у АСС ЩИ
            							      >	о А.
            
            (ЕО							      |	т ВИ _
            							      >	И „@ ес Днепр.
            
            							      >	       
             								 
            
             							      |	  
            							      >	      
            
            Одес							      |	       
            
             								 
            
             							      |	А-В
            
             								 
            
             								 
            
             								 
            							      <
             								 
            							      >	   
            							      >	  
            
             							      |	АВТОНОМНАЯ <
            							      >	РЕСПУБЛИКА
            							      >	КРЫМ
            							      >
            							      >	Границы и названия, приведенные на данной карте, а также испо
            							      >	ней обозначения, не отображают официального одобрения или сог
            

  
## other-tools

Depending of the percentage % of tricky-docs you got in PDF vs. the % you got in Paper , might dedicate more time to the other tool (scanned)

-  Digital-2Images- ...      is for PDFs received from partners (non-scanned)  (you notice when you can highlight text )

     - In those cases image extraction  process is deterministic - we can get all photos the producer embedded
     
- Scanned-2recognizeImages

     -  For Scanned from paper - the process of extracting images use another tool that is stochastic 
	
 -  Digital-2Images-2colorLight-2ocr
 
     - In any case, once the pictures are extracted, another process add more/less light , contrast, size.
	 - If we put all variables to user then it would be difficult to handle
	 - cureent trade-off  just automate a couple of contrast/light combinations so the user has 2 text options
	    
 -  zoom-in smaller images , enlarge
 
      - Other tool can enlarge the Smaller Images too for separated process
      - sometimes it works for quite small  logos but yet there are many variables
	    
- This tools can be set  on a secured sensible zone, or as mentioned standalone air-locked  box,  ( or upload in the secure zone  )

    - For high security environment, usually virtualization requires nvdia-gpu licenses for fast image processing
 



# Technical-steps 

## automated by Digital-2Images-2colorLight-2ocr

### 1. Listed & extracted large-enough Images found digitally-enclosed 

Automatically extracted large imagenes, un-touched, as were enclosed in a DigitallyCreated PDF.

	   pdfimages  -list  31stReportUkraine-ru.pdf  | grep -vE '\s[0-9]{1,4}B\s' 
	   pdfimages  -all   31stReportUkraine-ru.pdf     x 
	   mkdir u ; pdfimages  -list  31stReportUkraine-ru.pdf  | grep -vE '\s[0-9]{1,4}B\s' | awk '//{ printf "cp x-%03d-%03d.* u \n", $1, $2 } ' | sh

### 2. Created a 2nd version per image: As-found and after Tuning bright/color, 

Create a 2nd version of each image  with tuned-bright-color  (A simmplification for ease-use. for best results should try N-combinations of  bright contrast hue ... )

	   #convert-im6.q16 x-* -modulate 110,110,166 -background white  -normalize  -contrast-stretch 2% -sharpen 0x.2   -colorspace gray  -page a4   
		ls x* | xargs -l -I :   convert-im6.q16   :    -modulate 110,110,166   -background white  -normalize  -contrast-stretch 2% -sharpen 0x.2   -colorspace gray  -page a4  y:
			
### 3. Produced a PDF for other-OCRs with extracted images, zoomed + tuned  

Concat all zoomed tuned images in a PDF 
 
	   sudo mv /etc/ImageMagick-6/policy.xml /etc/ImageMagick-6/policy.xml.off
	   #   <policy domain="module" rights="read|write" pattern="{PS,PDF,XPS}" />
	   cd u ;  convert-im6.q16 yx-* -page a4    a4.pdf


### 4. Produced TXT from a free-OCR . Classified per page-image 

Applied free-OCR on the (extracted rotated) images, also in the tuned light-color version 

		ls x* | xargs -l -I :  tesseract -l rus --oem 3 --psm 3 : t:    # texts are tx-page-image ....
		ls yx* | xargs -l -I :  tesseract -l rus --oem 3 --psm 3 : t:    # texts are tyx-page-

		Tesseract Open Source OCR Engine v4.0.0-beta.1 with Leptonica
		Warning. Invalid resolution 0 dpi. Using 70 instead.
		Estimating resolution as 419
		Detected 3 diacritics
		Tesseract Open Source OCR Engine v4.0.0-beta.1 with Leptonica
		Warning. Invalid resolution 0 dpi. Using 70 instead.
		Estimating resolution as 507
		Tesseract Open Source OCR Engine v4.0.0-beta.1 with Leptonica
		Warning. Invalid resolution 0 dpi. Using 70 instead.
		Estimating resolution as 524
		Detected 10 diacritics
		Tesseract Open Source OCR Engine v4.0.0-beta.1 with Leptonica
		Warning. Invalid resolution 0 dpi. Using 70 instead.
		Estimating resolution as 287
		Detected 155 diacritics
		Tesseract Open Source OCR Engine v4.0.0-beta.1 with Leptonica
		Warning. Invalid resolution 0 dpi. Using 70 instead.
		Estimating resolution as 274
		Detected 49 diacritics
		Tesseract Open Source OCR Engine v4.0.0-beta.1 with Leptonica
		Tesseract Open Source OCR Engine v4.0.0-beta.1 with Leptonica
		Tesseract Open Source OCR Engine v4.0.0-beta.1 with Leptonica
		Tesseract Open Source OCR Engine v4.0.0-beta.1 with Leptonica
		Warning. Invalid resolution 0 dpi. Using 70 instead.
		Estimating resolution as 380
		Tesseract Open Source OCR Engine v4.0.0-beta.1 with Leptonica
		Warning. Invalid resolution 0 dpi. Using 70 instead.
		Estimating resolution as 406
		Image too small to scale!! (2x36 vs min width of 3)
		Line cannot be recognized!!


Listing russian text # compared tuned version per each page-image 
		ls x* | xargs -l -I :    diff -y  t:.txt ty:.txt 
 
		# for f in  tx*  ; do echo -e "### PAGE $f =========\\n" ; cat $f ; done
		for f in  x*  ; do echo -e "\n### PAGE-IMG $f " ; diff -y t$f.txt  ty$f.txt |  sed 's/^/            /'; done
 
and a HTM version

		cp ../fm.DigitallyComposed-to.Extracted.txt   ../ex.md
		 pandoc -f markdown ../ex.md -t html -o ../ex.htm





# ToC

_Table_

 [[_TOC_]]

