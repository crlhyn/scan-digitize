#!/bin/bash

# by C.J. 
#
#
if [[ -z "$1" ]] ; then
   echo usage: $0  file-t-scan
   echo usage: $0  file-t-scan   optional-lang 
   echo   e.g. $0  scanned.pdf    kaz 
   exit 1
fi

rus=${2:-rus}

ls='-l'  #for listing details
ls='' #simple list

b=$(basename $1)
d=${b//[: ]//}
dd=/tmp/d_$d


mkdir $dd   ||  echo Remember to clean /tmp regulerly, a simple reboot do that
cp "$1" $dd
cd $dd

echo "# $b in $rus" | tee  fin.txt
echo '<meta charset="UTF-8">' >> fin.txt #IMPORTANT
echo used $0 $* with temporal dir:  $dd === | tee -a  fin.txt

pdfimages -p -all "$b"  x   #includ page numbers


echo listed $(pdfimages -list  $b|wc -l) embedded images , including minimal unusefull,

echo List of decent sizes |tee -a fin.txt

 pdfimages  -list  $b | grep -vE '\s[0-9]{1,3}B\s' 

echo copying to $dd/u only the medium-large images >>fin.txt

mkdir u ; 
## this in some debian ubuntu: pdfimages  -list  $b | grep -vE '\s[0-9]{1,4}B\s' | awk '//{ printf "cp x-%03d-%03d.* u \n", $1, $2 } ' # | sh
pdfimages  -list  $b | grep -vE '\s[0-9]{1,4}B\s' | awk '//{ printf "cp x-*%03d.* u \n", $2 } '  | sh  ## CentOs RHEL + Ubuntu
ls $ls u/ |tee -a  fin.txt


echo For most precision will rather use ppm images , taking few seconds, | tee -a fin.txt
pdftoppm $b p
ls $ls p* 


echo Recognizing per Page  | tee -a fin.txt
# rus + eng no good  ls p*  | xargs -l -I :  tesseract -l $rus -l eng --oem 3 --psm 3 : t:
ls p*  | xargs -l -I :  tesseract -l $rus --oem 3 --psm 3 : t:
  
echo >>fin.txt
echo "## TEXT Follows " | tee -a fin.txt
  # find -name t\* -size +2c |sort
 more $(find -name t\* -size +2c |sort) | sed -E 's|:::::::+\s*$||;s|^./tp-([0-9]+).ppm.txt|\n### Pg \1 |' >> fin.txt

echo >>fin.txt
echo -e "\n## writing text to $1.md on $(date) , for .htm ..." | tee -a fin.txt
echo -e "\n latest version opensource at https://gitlab.com/crlhyn/scan-digitize " | tee -a fin.txt

cd -
cp $dd/fin.txt "$1.md"
pandoc -f markdown   "$1.md"   -t html -o "$1.htm"

#for russian 
#re   hunspell -d ru_RU -l $1 > /tmp/ru        #  usage     review  file  #just the typos
#re   grep --color -wf  /tmp/ru $1       $2   #  or usage     review  file  -C99   # full text
