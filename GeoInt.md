# Example of connected countries

```plantuml
	
@startyaml
#highlight "AGO"
#highlight "VEN" / "gri"

AFG: 
  img: htds/2021/story/images/big/over-260-afghan-sikhs-in-kab
  href: "olders/1rXwLCNR70qqEHFbowqRkoNc7fcVz0NHm?jc=AFGANIST"

  rel: 1u  AFGANISTAN/AFG- Discriminación SIKS
  
AGO:
   href: folders/1hVmvR1WdaetlUHc0RNTELpIfG-E1TlQJ?jc=ANGOLA"
   gri: 1   Persecuciones de niños con discapacidad en Angola
   img: hgo.com/iu/?u=https%3A%2F%2Ftse2.mm.bing.net%2Fth%3Fid
    
ARM:
   href: folders/1AVbRXP7BhjNUppPVtMax-tB8RsSlKeUZ?jc=ARMENIA'
   pol: 1   ARMENIA/Corrupción política en Armenia  DIR
   etn: 1   GEORGIA-ARMENIOS+PARTIDO SUEÑO GEORGIANO
   


BFA:
 href: 'hlders/1Q_5BYvB-VQ3z8yJo4VY1s6YrxF1DUDOf?jc=BURKINA'
 img:    76/mcs/media/images/65456000/jpg/_65456065_islamistre

 grv: 1qO BURKINA FASO/Terrorismo
    


BGD:  #ba
 img:  "hP.AavYVST-i_gNTG6hQURzLQHaE7&pid=Api"
 rel: 1oN BANGLADESH/BUDISTAS
 pol: 1sZ BANGLADESH/OPOSITORES
   


CIV:
   href: folders/14c3l1e2ZMLRKEg5J0hL65C1XXnRzGyKV?jc=COSTADE'

   etn: 1   COSTA DE MARFIL/RACI


CMR:
  href: 'olders/1_aTF39s_HSq7FihrssCevDOhMLcPFpB4?jc=CAMERUN'
 
  gri: 1V  CAMERÚN/GÉNERO                 DIR
  grs: 1x  INFORME HOMOSEXUALIDAD EN CAMERÚN
  gra: 1u  CAMERÚN/PERSECUCIÓN ANGLÓFONO  DIR
 


COD:
   href: folders/1ou-t5I7V-LU28UEV3WZYhth9XQqRtwXn?jc=R.DCONG'
   wb: ZA
   gra: 1   R.D CONGO/SITUACIÓN DDHH 
   img:  kgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fi
   
    
COL:
 href: 'hlders/1Dd46RZ4WnSSZz12zmIqqcaE_ARDcywQq?jc=COLOMBIA'
 img: htty/f3ff8268-3365-11eb-92ff-005056bf87d6/w:1280/p:16x9/
 busq: ye
 grv: 1UF COLOMBIA/Extorsiones Bucaramanga
 gra: 1x3 COLOMBIA/Falsos Positivos                  
 grv: 1Id COLOMBIA/Impunidad y Violencia              
 grs: 1UP COLOMBIA/Minorias - Mujeres y  LGTBI+       
 grv: 1ge COLOMBIA/Pandillas Oriente de Cali          
 grv: 1ff COLOMBIA/Plan Pistola                       
 nat: 1uD COLOMBIA/Problema fronterizo con Venezuela  
 gri: 1Dw COLOMBIA/Violencia de Género
 gri: 1u0 COLOMBIA/La verdad de la mujeres (VDG)
 gri: 1su COLOMBIA/Abuso sexual a menores
    
CUB:
  href: 'olders/1CMFrzeO2rx4Cs7ZtXdAqlo6FVz_q9Yzs?jc=CUBA'

  pol: 1v  CUBA/Opositores  DIR
   


DZA:
  href: "olders/1HDeBTiRfvJ01Qz_ZXZ9WiCSBEDVcCngw?jc=ARGELIA"

  gra: 1q  ARGELIA/Amenazas y persecución a empleados empresa
  pol: 1Z  ARGELIA/Persecución a opositores
  grv: 1i  ARGELIA/Persecución a periodistas

  gri: 1g  FRONTERA MALI Y ARGELIA/Campamentos esclavas sexual
  nat: 1g  FRONTERA MALI Y ARGELIA/Campamentos esclavas sexual


EGY:
  href: hlders/1uVIl_e4-5T9VULUoe1oD6VwscXevGWnL?jc=Egip
  rel: 1J  EGIPTO/EGIPTO-SITUACIÓN CRISTIANOS
  pol: 1k  EGIPTO/FAMILIA HAMADIYA_
 

ERI:
   href: folders/1t7HqQaM8ferTKQ8i7S_Yj9shFbx18WRD?jc=ERITREA'
   grv: 1   ERITREA/Alistamiento militar obligatorio. Consecue
   img: hgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid
   
    
ESH:
   img:  ckgo.com/iu/?u=https%3A%2F%2Fi.pinimg.com%2F736x%2F80
   pol: 1     Movimiento Gulen + Internacional

     
GEO:
 href: 'hlders/17AtyuhxWdoL9SVd9IFjC6xgeyEFtZiMC?jc=GEORGIA'

 etn: 1ne GEORGIA-ARMENIOS+PARTIDO SUEÑO GEORGIANO
 grs: 1Oy Georgia  LGTBI+ 
 pol: 1Oy Georgia - Oposición 
 gri: 1bV GEORGIA VDG
    
GHA:
   href: folders/1p2udlEEPlUt5d_brZl_cqpvmrAxGs8eL?jc=GHANA'
   img: hgo.com/iu/?u=https%3A%2F%2Ftse4.explicit.bing.net%2Ft
   img: hgo.com/iu/?u=https%3A%2F%2Ftse2.mm.bing.net%2Fth%3Fid
   etn: 1   GHANA/grupo étnico Akan o Twi
   gri: 1   GHANA/Sacrificios Humanos rituales
   
GIN:
 href: 'hlders/1aC7mWRTbMHrrJAS60t2NAZ7yb7aAAf6k?jc=GUINEAC'

 rel: 1rw GUINEA CONAKRY/CONVERSOS AL CRISTIANISMO
 grv: 1-J GUINEA CONAKRY/GUINEA - ACTOS DE CANIBALISMO
 etn: 1rE GUINEA (CONAKRY)_ PROBLEMATICA ENTRE LAS DISTINTAS E
 pol: 1tJ GUINEA CONAKRY/Opositores y Situacion politica
   
GMB:

   grs: 1   GAMBIA/COLECTIVO LGTBI
   href: folders/1qIohCv-IBoDuLUwGjZPwRCLpcVwl4L0h?jc=GAMBIA'

GNQ:

   href: folders/18FCrUwKhOUGNMcuwTHb13VM6oKZL4unr?jc=GUINEAE'



HND:
   img: hP.3IpHSjezGoMSpm2Au4POVwHaEz&pid=Api
   href: folders/1RPaXShHcQIRWxCvqY98gvHmUOPOJc25C?jc=HONDURAS
   d: 2
   busq: 

   gri: 1   HONDURAS/HONDURAS - VDG

   pol: 1   HONDURAS/MOVIMIENTO CAMPESINO
   grs: 1   HONDURAS/Persecución LGTBI
   grv: 1   Violencia MARA Honduras
    

IRN:
   href: folders/1mtZv6WfOZPWjNE4-FkHBCqfDSTqQlbG6?jc=IRAN'

   gri: 1   IRAN/SITUACIÓN MUJER
  

IRQ:
   href: folders/1EcXEy4eN2TaHksSTukabVrMQ85XSglOR?jc=IRAK'
 

   rel: 1   Persecución a la comunidad Sunní en Irak
   nat: 1   IRAK/Position on Returns to Iraq 
    


JAM:
   href: folders/1BEx4zEf5VM6iIOU1VZ9fsJL2d3v_-gNz?jc=JAMAICA'
 
   grs: 1    JAMAICA LGTBI
  

JOR:
   href: folders/1n1v5x0Xrk2O17p4aL1K-oGx8ZZnhJWKf?jc=JORDANIA
   rel: 1   JORDANIA/Situación libertad religiosa en Jordania

    

LBN:
   img:  kgo.com/iu/?u=https%3A%2F%2Ftse2.mm.bing.net%2Fth%3Fi
   href: folders/1I9panW_cDG5zGI6BWoxhM1iBZfFjoa9P?jc=LIBANO'
   etn: 1   LÍBANO/LÍBANO - PROBLEMAS RELIGIOSOS Y ÉTNICOS
   rel: 1   LÍBANO/LÍBANO - PROBLEMAS RELIGIOSOS Y ÉTNICOS

    

MAR:
   img: "IP.pwgl63-BCy7kEFcjgE3fbwHaE7"
   href: folders/1MFUT5iYubOYIFgKbL9KNaKsAe23vBnvA?jc=MARRUECO
   rel: 1   MARRUECOS/CRISTIANOS COVERSOS
   gri: 1   MARRUECOS/VIOLENCIA DE GÉNERO
   pol: 1   TURQUÍA - MARRUECOS/Movimiento Gulen

   busq: 
   

MLI:
   href: folders/1Gh46mrZDQ8AtVJ4aCy5GOyovxYdXnL79?jc=MALI'
   nat: 1 Deportaciones 
   etn: 1 Esclavitud Etnias
   rel: 1 Persecución a cristianos
   pol: 1 Golpe de Estado

   img: hnt/uploads/sites/3/2018/03/peacekeeping-mail-4-dead.j
   busq: 

   gri: 1   FRONTERAs/Campamentos esclavas sexuales

 
   
MMR:
   img: hset/5bed96803c000091030eb9b0.jpeg?ops=1200_630
   href: folders/1m_ig65p1g3i2xnTwJMfO5XN3yK4xwc2c?jc=MYAMAR'
   wb: BU
   rel: 1   MYAMAR/Rohinya
  
    
 
MRT:
   img: hgo.com/iu/?u=https%3A%2F%2Ftse2.mm.bing.net%2Fth%3Fid
   grv: 1   MAURITANIA/YIHADISMO NORTE PAÍS
   rel: 1   MAURITANIA/YIHADISMO NORTE PAÍS
    
   href: folders/14kCBmlkHWhjulRr7cauz5Ewz5rK81eUT?jc=MAURITAN

NGA:
   href: folders/1C7TtNMXUt_Wgp27YSAcS2J9dl_I_25F1?jc=NIGERIA'
   grv: 1   NIGERIA/Boko Haram  DIR
   rel: 1   NIGERIA/Boko Haram  DIR
   grs: 1   NIGERIA/LGTBI       DIR
   img: hgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid
   
   

NIC:
   href: folders/1zwEWAo97smNYNWy95mPjZrlMEVB6Rm_-?jc=NICARAGU
   busq: 

   grs: 1   NICARAGUA/LGTBIQ+
   gra: 1   NICARAGUA/PERSONAL UNIVERSITARIO
   



PAK:
   img:  /vtg-internet/en/aktuell/einsaetze-und-operationen/mi
   href: folders/11NzkXKaWw0bm0nN2XEYO6b5y8E4ckhf5?jc=PAKISTA'
   grv: 1   PAKISTÁN/Situación Bélica Cachemira
  

PER:
   href: folders/15rw8QlfQoosIh2aj0ggR0_ce63Uktaj_?jc=PERU'
   grv: 1   PERU/Extorsiones a comerciantes
  
    

PHL:
   href: folders/1BerZUek1-Zl34nmGB8vYbsEw8_IELJaD?jc=FILIPINA
 

PRY:
   href: folders/1l4SixdEIKescRBvRTWbqnOrrEsAgSCda?jc=PARAGUAY
   grs: 1   PARAGUAY/SITUACIÓN DEL COLECTIVO LGTBI
   gri: 1   PARAGUAY/VIOLENCIA DE GENERO E INFANTIL

    


PSE:
   href: folders/1oYsxzspDgIELzPX-d-reTqeOFQpgOCQs?jc=PALESTIN
   wb: IS
   nat: 1   PALESTINA/OCUPACIÓN TERRITORIOS
 

RUS:
   img: hP.waFcdyzo-sjdamtY5e1ewQHaD4&pid=Api
   href: folders/19qQgPyTKcE3uoWQ92UNzHWQSYLXaqqau?jc=RUSIA'

   nat: 1   RUSIA/Chechenia              DIR
   etn: 1   RUSIA/Discriminación etnica  DIR
   pol: 1   RUSIA/Libertad de expresión  DIR
   rel: 1   RUSIA/Religión y Violencia   DIR
   gri: 1   RUSIA/Violencia Género       DIR
   grs: 1   RUSIA/LGTBI                  DIR
   grs: 1 Persecución por orientación sexual
   

SAU:
   href: folders/1Q6E9wKTxEBZNdo_VdBYUWz-YzK7AZzUr?jc=ARABIAS"

   gri: 1   ARABIA SAUDI/Situación mujeres
  

SDN:
   href: folders/12NBU7vzPcfGNiB55W9lOUnvNa2lIRByA?jc=SUDAN'
   img: hnt/uploads/2021/05/Refugees-in-the-Um-Baru-camp-North
   etn: 
   nat:
   grph: 
   grpa: 
   grpi: 
   pol: 
   rel: 
    

SEN:
   href: folders/1dYY7sbF4NvhZC8iChIrt8NCVaf_ndR4X?jc=SENEGAL'
   busq: 
   d: 2
   href: folders/1r5wAXNwSxR0BySZovDIlN3U1gvGKlwhw?jc=SENEGAL'

    
   grv: 1    Revueltas en Senegal
   etn: 1    SENEGAL _THIANTOCOUNE_

   img:  kgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fi
   


SLV:
  href: hlders/1HuoMGUPU_-4zKS3Kw_Ac5zfbDEzxPX0N?jc=ELSALVA
  busq: y

  grs: 1I  EL SALVADOR/Persecución colectivo LGTBI
  gri: 1w  EL SALVADOR/Violencia de Género
   
   
SOM:
   img: hgo.com/iu/?u=https%3A%2F%2Ftse4.mm.bing.net%2Fth%3Fid
   href: folders/1rQNynjTW6XGSPeyznQ61VuUF8ipcO-oz?jc=SOMALIA'
   grv: 1   SOMALIA/CONFLICTO ARMADO
  

SSD:
   img: hgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid
   href: folders/1DvcDWkeq9fI9rsgrInXcWEmvgFJvjqXO?jc=SUDAND'
   rel: 1   SUDÁN DEL SUR/Discriminaciones Religiosas
   grv: 1    Loaded Guns and Empty Stomachs  
  
    
SYR:
   img:  kgo.com/iu/?u=https%3A%2F%2Ftse2.mm.bing.net%2Fth%3Fi
   href: folders/1JFExg8EKK7XJx_6HeXfpKQWJOIe52feY?jc=SIRIA'
   rel: 1    PERSECUCIÓN A CRISTIANOS EN AL QAYATAIN
 
    
TCD:
   href: folders/1heiKpShfstq8zJcjJ0AOY3_gUw1bKvu5?jc=Chad'
   img: hmages/tuesday/levghglc0yhiny60751f552c0d2.jpg
   grv: 1   CHAD/Ataques de rebeldes



TUN:
   href: folders/1Xe_rOXbXC7Tdkd2ucgsDk-0STDoNxpYw?jc=TUNEZ'
   img:  IP.kgPrY9X_qwNrfcLq1YH7CgHaEK&pid=Api
   grs: 19d11lz6zE94   SITUACION MATRIMONIOS  
   
    


TUR:
   href: folders/1okj7rRy9nSZpIJCk_3A1fDKiM_BoY9mo?jc=TURQUIA'
   img: hP.0NoRBkCsf1kN7-mMDJaolwHaD2&pid=Api
   pol: 1   TURQUÍA - MARRUECOS/Movimiento Gulen
   
  
VEN:
   href: folders/1Wu402DGrX5j-9qxLapiu8nKTmoP3yecB?jc=VENEZUEL
   busq: 
   img: h2018/02/22/452145_568878.jpg
   etn: 1   Minoría china                                DIR
   gri: 1   Falta medicinas                            DIR
   gra: 1   Prisiones/Funcionarios                         DIR
   grs: 1   LGTBI+                                       DIR
   pol: 1   Adoctrinamiento                              DIR
   grv: 1   VENEZUELA/Soldados Franelas                       


YEM:
   etn: 
   img: hgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid
   pol: 1   YEMEN/ Persecución a los liberales  DIR
   rel: 1   YEMEN/Libertad religiosa   DIR



@endyaml

```
