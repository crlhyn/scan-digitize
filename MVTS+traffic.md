<style> img  {   width: 70%;   border: none;   background: none; </style>

## Money Value Flow examples

examples based on published open sources including UNODC, OCDE, FATF, 
- https://www.fatf-gafi.org/media/fatf/documents/reports/Financial-flows-linked-to-production-and-trafficking-of-afghan-opiates.pdf


### Opiate

We historic flows 2014 : Case study 1 ‘Triangular’ opiate-related financial flows between MVTS operators

```plantuml
object "UK" as a
object "Pakistan" as b
object "Afghanistan " as c
object "UAE " as d

a : <img:https://upload.wikimedia.org/wikipedia/commons/1/1a/UK-geo-stub.png{scale=0.3}>
b : <img:https://upload.wikimedia.org/wikipedia/commons/d/dd/Pakistan-geo-stub.png{scale=0.5}>
c : <img:https://upload.wikimedia.org/wikipedia/commons/b/bc/Flag_map_of_Afghanistan.png{scale=0.1}>
d : <img:https://upload.wikimedia.org/wikipedia/commons/0/0d/Flag-map_of_UAE.png{scale=0.3}>

a -> b
b -> c
c -> d
d -> c
```



```mermaid
sequenceDiagram
    participant UK
    participant Pakistan
    participant Afghanistan
    participant UAE
    
        UK->>Pakistan: Telegraph Payment
		
    loop back-forth
        Afghanistan->>Afghanistan: internal flows
    end
    		Pakistan->>Afghanistan: MVTS Payment
		Afghanistan->>UAE: pay for goods
		UAE->>Afghanistan: goods imported

    Note right of UAE: triangle

    Note right of UK: one step further would hide the triangle  
```


```mermaid
pie title traceable hints per area
	"UK" : 4
	"Pakistan" : 5
	"Afghanistan" : 7
  "UAE" : 3
```
<img src="https://upload.wikimedia.org/wikipedia/commons/b/bc/Flag_map_of_Afghanistan.png" width="120">


```mermaid
graph LR
    A[UK a] -->|telegram pay| B(PAK)
    C[UK b] -->|telepay| B
    B -->|Transaction| D(AFG)
    D -->|Status change| E(UAE)
    E --> D
```
   
```mermaid
graph LR
    A[UK] -- Telepay --> B(Pakistan)
    B  -- MVTSpay --> C
    C(Afghanistan )
    C-->D(UAE)
    D-->C 
```


```plantuml
@startuml
: tab
----
* image x0.5 : <img:https://upload.wikimedia.org/wikipedia/commons/b/bc/Flag_map_of_Afghanistan.png{scale=0.1}>
----
;
 
@enduml
```
### Cyber

Another historic flow 2016 by international gambling and blockchains:

```mermaid

gantt
 title  I Gantt diagram
    dateFormat  YYYY-MM
    section Inspect 1
    Research & requirements :done, a1, 2021-08, 2022-01
    Review & documentation : after a1, 120d
    section Inspect 2
    Implementation      :crit, active, 2021-05  , 120d
    Testing      :crit, 120d
 ```   



<img src="https://upload.wikimedia.org/wikipedia/commons/b/bc/Flag_map_of_Afghanistan.png"  width="120" >



- [x] Completed task
- [ ] Incomplete task
  - [ ] Sub-task 1
  - [x] Sub-task 2
  - [ ] Sub-task 3
  


   
###  Russia   

src  Russia   Federation & FATF

    
```mermaid
graph TB

  A --> B
  subgraph "SubGraph 1 Flow"
  B(Kyrz 1)
  B -- Choice1 --> DoChoice1
  B -- Choice2 --> DoChoice2
  end

  subgraph "Main Graph"
  Node1[ 1] --> Node2[ 2]
  Node2 --> A[Russia Fed]
  A --> FinalThing[Final Thing]
end
```


 


## Other ongoing 

https://app.diagrams.net/?lightbox=1&highlight=0000ff&edit=_blank&layers=1&nav=1&page=3&title=#Uhttps%3A%2F%2Fraw.githubusercontent.com%2Fjgraph%2Fdrawio-diagrams%2Fmaster%2Fblog%2Fmermaid-examples.drawio



```mermaid
graph TB
    sq[Square shape] --> ci((Circle shape))

    subgraph A
        od>Odd shape]-- Two line<br/>edge comment --> ro
        di{Diamond with <br/> line break} -.-> ro(Rounded<br>square<br>shape)
        di==>ro2(Rounded square shape)
    end

    %% Notice that no text in shape are added here instead that is appended further down
    e --> od3>Really long text with linebreak<br>in an Odd shape]

    %% Comments after double percent signs
    e((Inner / circle<i>and some odd </i>special characters)) --> f(,.?!+-*ز)

    cyr[Cyrillic]-->cyr2((Circle shape Начало));

     classDef green fill:#9f6,stroke:#333,stroke-width:2px;
     classDef orange fill:#f96,stroke:#333,stroke-width:4px;
     class sq,e green
     class di orange
```

```plantuml
@startuml

!include <aws/common>
!include <aws/Storage/AmazonS3/AmazonS3>
!include <aws/Storage/AmazonS3/bucket/bucket>

AMAZONS3(s3) {
    BUCKET(site,www.insecurity.co)
    BUCKET(logs,logs.insecurity.co)
}

site .r.> logs : events

@enduml
```

```mermaid

graph TD
    setup -->|a b| PandHTM

```
