

### web

    sudo docker run -dit --name web -p 8080:80 -v apache2/htdocs/:/usr/local/apache2/htdocs/ httpd:2.4

docker.html    # content to file.

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>  Apa Dock  </title>
    </head>
    <body>
        <h1>  Apa Dock </h1>     https://gitlab.com/crlhyn/scan-digitize
    </body>
    </html>


### comp_ng


docker-compose.yml # with only the reverse proxy

    version: "3.3"
    services:
    nginx-proxy:
        image: jwilder/nginx-proxy
        ports:
        - 80:80
        volumes:
        - /var/run/docker.sock:/tmp/docker.sock:ro


### comp_ws

service1/docker-compose.yml

    version: "3.3"
    services:
    php1:
        ...
        networks:
        - backend1
    apache1:
        ...
        networks:
        - nginx-proxy_reverse-proxy
        - backend1
        environment:
        - VIRTUAL_HOST=w1.localdomain
    mysql1:
        ...
        networks:
        - backend1
    networks:
    backend1:
    nginx-proxy_reverse-proxy:
        external: true


Dockerfile

    FROM nginx:alpine
    COPY nginx.conf /etc/nginx/nginx.conf
    RUN apk update && apk add bash

then
    docker build -t nginx-al  .
    docker run -ti nginx-al  /bin/bash

our docker-compose.yml:

    services:
        reverseproxy:
            image: reverseproxy
            ports:
                - 8080:8080
                - 8081:8081
            restart: always
    
        nginx:
            depends_on:
                - reverseproxy
            image: nginx:alpine
            restart: always
    
        apache:
            depends_on:
                - reverseproxy
            image: httpd:alpine
            restart: always

docker-compose up -d
